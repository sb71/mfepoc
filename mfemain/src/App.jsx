import React from "react";
import ReactDOM from "react-dom";
import { SimpleGrid, Text, ChakraProvider } from "@chakra-ui/react";
import { load } from "./store";

load("people");

import Search from "./components/Search";
import ListAll from "./components/ListAll";
import Favorite from "./components/Favorite";

import "./index.css";
const H3 = ({ children }) => (
  <Text fontSize="xl" mb={3} fontWeight="bold" textAlign="center">
    {children}
  </Text>
);

const App = () => (
  <ChakraProvider>
    <SimpleGrid
      columns={[1, 1, 3]}
      spacing={10}
      m={[1, 1, 6]}
      templateColumns="1fr 3fr 1fr"
      gap={1}
    >
      <div>
        <H3>Search</H3>
        <Search />
      </div>
      <div>
        <H3>Charecters</H3>
        <ListAll />
      </div>
      <div>
        <H3>Favorite</H3>
        <Favorite />
      </div>
    </SimpleGrid>
  </ChakraProvider>
);

ReactDOM.render(<App />, document.getElementById("root"));