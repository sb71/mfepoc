import React from 'react';
import { useSnapshot } from "valtio";
import store from "../store";

const DataComponent= ({ children }) => {
  const state = useSnapshot(store);
  return children(state);
};

export default DataComponent;