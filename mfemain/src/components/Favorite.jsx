import React from "react";
import { Box, Button, Heading, Avatar, Center, useColorModeValue } from "@chakra-ui/react";
import { useSnapshot } from "valtio";

import store from "../store";

const Favorite = () => {
  const { favorite } = useSnapshot(store);
  return (
    <React.Fragment>
      <Box>
        {favorite.map((person, i) => (
          <Center py={6} key={person.name.split(" ").join("-")}  mt={8}>
            <Box>
              <Heading fontSize={"2xl"} fontFamily={"body"}>
                {i+1} {'. '}{person.name}
              </Heading>
            </Box>
          </Center>
        ))}
        <Box>
          <Button width="100%" onClick={() => (store.favorite = [])}>
            Checkout
          </Button>
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default Favorite;
