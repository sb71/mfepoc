import React from "react";
import {
  FormControl,
  FormLabel,
  Input,
  Slider,
  SliderTrack,
  SliderFilledTrack,
  SliderThumb,
  Box,
} from "@chakra-ui/react";
import { useSnapshot } from "valtio";

import store, { setSearchText, setHeight } from "../store";


const Search = () => {
  const { searchText, height } = useSnapshot(store);
  return (
    <Box >
      <FormControl id="search">
        <FormLabel>Search</FormLabel>
        <Input
          type="text"
          value={searchText}
          onChange={(evt) => setSearchText(evt.target.value)}
        />
      </FormControl>

      <FormControl id="height">
        <FormLabel>Height</FormLabel>
        <Slider
          colorScheme="pink"
          defaultValue={height}
          onChange={(v) => setHeight(v)}
          min={0}
          max={200}
        >
          <SliderTrack>
            <SliderFilledTrack />
          </SliderTrack>
          <SliderThumb />
        </Slider>
      </FormControl>
    </Box>
  );
};

export default Search;