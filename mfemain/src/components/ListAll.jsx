import React from "react";
import { Grid } from "@chakra-ui/react";
import { useSnapshot } from "valtio";

import store from "../store";
import Person from "./Person";

const ListAll = () => {
  const { filteredPeople } = useSnapshot(store);
    console.log(filteredPeople);
  return (
    <Grid templateColumns="repeat(3, 1fr)" gap={6}>
      {filteredPeople.map((person) => (
        <Person key={person.name.split(" ").join("-")} person={person}/>
      ))}
    </Grid>
  );
};

export default ListAll;