import { proxy, subscribe as valtioSubscribe, snapshot } from "valtio";

const store = proxy({
  people: [],
  searchText: "",
  height: 190,
  filteredPeople: [],
  favorite: [],
});

const filter = () => {
  const searchRE = new RegExp(store.searchText, "i");   
  return store.people.filter(({ name, height }) => {
        console.log("Filtering people by search text:", name, height);
        return name.match(searchRE) && parseInt(height) <= store.height
        });
};

export const load = (client) => {
  fetch(`http://localhost:8080/${client}.json`)
    .then((resp) => resp.json())
    .then((people) => {
      store.people = people;
      store.filteredPeople = filter();
    });
};

export const setSearchText = (text) => {
  store.searchText = text;
  store.filteredPeople = filter();
};

export const setHeight = (height) => {
  store.height = height;
  store.filteredPeople = filter();
};

export const makeFavorite = (people) => {
  store.favorite.push(people);
};

export const subscribe = (callback) => {
  callback(snapshot(store));
  return valtioSubscribe(store, () => callback(snapshot(store)));
};

export default store;
