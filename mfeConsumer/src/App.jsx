import React from "react";
import ReactDOM from "react-dom";
import {
  extendTheme,
  ChakraProvider,
  SimpleGrid,
  Text,
  Image,
} from "@chakra-ui/react";

import "./index.css";

import Search from "mfemain/Search";
import Favorite from "mfemain/Favorite";
import ListAll from "mfemain/ListAll";
import DataComponent from "mfemain/DataComponent";
import { load } from "mfemain/store";

load("people");

const config = {
  useSystemColorMode: false,
  initialColorMode: "dark",
};
const customTheme = extendTheme({ config });
const App = () => (
  <ChakraProvider theme={customTheme}>
    <div
      style={{
        maxWidth: "1440px",
        margin: "auto",
        display: "grid",
        gridTemplateColumns: "1fr 3fr",
        gridColumnGap: "1rem",
      }}
    >
      <div>
        <Search />
        <Favorite />
        <DataComponent>
          {({ filteredPeople }) =>
            filteredPeople.slice(0, 5).map((person) => (
              <SimpleGrid columns={2} templateColumns="1fr 7fr" gap={1} mt={3}>
                <div>
                  <Image src={`http://localhost:8080/images/${person.image}`} alt={`${person.name} logo`} />
                </div>
                <div>
                  <Text fontSize="xs" isTruncated>
                    <strong>{person.name}</strong>
                  </Text>
                </div>
              </SimpleGrid>
            ))
          }
        </DataComponent>
      </div>
      <ListAll />
    </div>
  </ChakraProvider>
);

ReactDOM.render(<App />, document.getElementById("root"));
